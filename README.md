# Financial Tools

These are tools I have made to make my life easier, and have been useful enough that I have felt like sharing them.

## Disclaimer

> These tools and calculators are provided for your information and to illustrate scenarios within an Australian context. The results should not be taken as a substitute for professional advice. I am not a Tax Accountant or a Financial Advisor, and do not have any Financial Services experience. 

> All reasonable care has been taken in preparing and designing the calculators and tools; however, I provide no warranties and makes no representation that the information provided by the calculators and tools is appropriate for your particular circumstances or indicates you should follow a particular course of action.

> You should consider seeking independent legal, financial, taxation or other advice to confirm the data or information you gain from the usage of these tools.

> I am not liable for any loss caused, whether due to negligence or otherwise arising from the use of, or reliance on, the information provided directly or indirectly, by usage of these tools/calculators

## List of Tools 

- Calculated Tax Deductions (SpreadSheet)
- Running Costs (SpreadSheet)

## Usage

### Calculated Tax Deductions

This was first made when I was doing on-call work, and was first used for tracking Time in Lieu usage. It has gradually evolved into the tool I use now to assist me in doing my annual tax returns.

This should be fairly easy to use, as it tries to directly reference ATO wording and rules where possible. Enter in your data in the Gold highlighted cells.

Dummy data has been used to populate the spreadsheet, feel free to modify as you see fit.

The file was originally created in Libre Office, I have saved it both as the original .ods file as well as a .xlsx file.

### Running Costs

This is useful for making budgets, and working out how much money you need to allocate to cover your exact expenses. Enter in your data in the Gold highlighted cells. 

Can also track historical income, which can be useful for tracking pay rises over the years. Also allows you to see how much your Emergency Fund can cover you in several scenarios.

Dummy data has been used to populate the spreadsheet, feel free to modify as you see fit.

The file was originally created in Libre Office, I have saved it both as the original .ods file as well as a .xlsx file.
